package com.example.notasrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.notasrecycler.adapter.NotasAdapter
import com.example.notasrecycler.modelo.Nota

class MainActivity : AppCompatActivity() {
    lateinit var recyclerNotas: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        recyclerNotas = findViewById(R.id.listaNotas)

        val notas = ArrayList<Nota>()
        notas.add(Nota(titulo = "Hacasdfasfasdfasdfadsfasdfasasasasasasasasasasassssssssser esto", descripcion = "Hacer que jale esto"))
        notas.add(Nota(titulo = "Hacer 1", descripcion = "Hfasdfadsfasdfasale esto"))
        notas.add(Nota(titulo = "Hacer 2", descripcion = "Hacasdfadsf jale esto"))
        notas.add(Nota(titulo = "Hacer 3", descripcion = "Hafdafdsfale ejafdskjfalsdkjfasdfkjasdflsakdjfañsdfjañsldkjfasñldkfjdsñjdslñkjfadlsñkjfañlsdkfjadlsñkjfñadlkssto"))
        notas.add(Nota(titulo = "Hacer 4", descripcion = "Hacer que jale esto"))
        notas.add(Nota(titulo = "Hacer 5", descripcion = "Hacasdfasdfto"))
        notas.add(Nota(titulo = "Hacer 6", descripcion = "Hacer queasdfasdf"))

//        recyclerNotas.layoutManager = GridLayoutManager(this,2)
        recyclerNotas.layoutManager = StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL)
        recyclerNotas.setHasFixedSize(true)
        recyclerNotas.adapter = NotasAdapter(notas, this)
    }
}